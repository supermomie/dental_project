import os
import re
import json
import fnmatch
import datetime
import pandas as pd
import numpy as np
from cv2 import cv2




DATASET_PATH = './dataset/cat1/cropped_images/images'
DATASET_PATH_SAVE = './dental_project/dataset/cat1'
PATH_DIR_SEG = './dataset/cat1/cropped_images/annotated_images/annotations'

images_path = os.listdir(DATASET_PATH)
images_path = fnmatch.filter(images_path, '*.jpg')
arr_dict_images_train = []
arr_dict_images_val = []
arr_dict_annotation_train = []
arr_dict_annotation_val = []
ANNOTATION_ID = 0



INFO = {
    'description': 'TEETH DATASET',
    'url' : 'https://gitlab.com/supermomie/dental_project',
    'version': '0.1.0',
    'year': 2020,
    'contributor': 'Fakhredine',
    'date_created': datetime.datetime.utcnow().isoformat(' ')
}

LICENSES = [{
    'id': 1,
    'name': 'Apache License',
    'url': 'https://gitlab.com/supermomie/dental_project/-/blob/master/LICENSE.txt'
}]

cat_name = pd.read_table("./class_list.txt", header=None)
cat_name = cat_name.values.tolist()
CATEGORIE = []
for name in cat_name:
    index = re.findall("\d+", name[0])
    name = re.findall("[a-zA-Z]+", name[0])
    info = {
        "supercategory": "teeth",
        "id": int(index[0]),
        "name": name[0]
    }
    CATEGORIE.append(info)

CATEGORIES = [{
    "supercategory": "teeth",
    "id": 1,
    "name": "dent"
    }]
CATEGORIES.extend(CATEGORIE)



def bi_mask(image_id):
    """
    param image id : image id
    return all masks and bbox on each image
    """
    all_bbox = []
    all_bimask = []
    image = cv2.imread(PATH_DIR_SEG+"/"+str(image_id)+".bmp")
    image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    _, thresh = cv2.threshold(image_gray, 0, 255, 0)
    contours, _ = cv2.findContours(thresh, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    for i in range(len(contours)):
        cnt = contours[i]
        x, y, w, h = cv2.boundingRect(cnt)
        all_bbox.append((x, y, w, h))
        img_cont = np.zeros(image.shape, dtype=image.dtype)
        cv2.drawContours(img_cont, contours, i, (255, 255, 255), -1)
        bin_mask = np.zeros(img_cont.shape[0:2], dtype=np.uint8)
        bin_mask = img_cont[:, :, 0]
        all_bimask.append(bin_mask)

    return all_bimask, np.array(all_bbox)



def create_image_info(image_id, file_name, image_size,
                      date_captured=datetime.datetime.utcnow().isoformat(' '),
                      license_id=1, image_url=""):
    """
    return dictionnary image
    """
    dict_images = {
        "id": image_id,
        "file_name": file_name,
        "width": image_size[1],
        "height": image_size[0],
        "date_captured": date_captured,
        "license": license_id,
        "image_url": image_url,
    }
    return dict_images



def build_coco_format(images, annotations, dataset_path, subset_dir,
                      info=INFO, licenses=LICENSES, categories=CATEGORIES):
    """
    Build dataset annotations in COCO Dataset format
    """
    json_data = {
        'info': INFO,
        'licenses': LICENSES,
        'categories': CATEGORIES,
        'images': images,
        'annotations': annotations
    }
    #print(json_data)
    dir_annotations = os.path.join(dataset_path, 'annotations')
    if not os.path.exists(dir_annotations):
        os.makedirs(dir_annotations)
    path_json = os.path.join(dir_annotations, 'instances_{}.json'.format(subset_dir))
    with open(path_json, 'w') as all_data:
        json.dump(json_data, all_data)
    print("JSON created...OK in:  %s", path_json)
    return json_data
