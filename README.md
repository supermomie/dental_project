[//]: # (Image References)

[image1]: ./path "HLS"

[link1]: https://github.com/IvisionLab/dental-image#user-content-request-dataset "Link of dataset request"
[link2]: https://docs.opencv.org/ref/2.4.13.3/d9/df4/structcv_1_1gpu_1_1device_1_1color__detail_1_1RGB2HLS.html "RGB2HLS"

# README


### Installing all libraries
`pip install -r requirements.txt`

# Train

`cd tensorflow`

`python train.py`

# Collecting Data


Data was requests from *IvisionLab* [Link of dataset request][link1]


# Preprocessing

For the preprocessing I used HLS filter and I resize it

*COCO file is comming soon with segmentations, bounding boxs, classes, ids ...*

- ### HLS  

    [Opencv DOC H L S][link2]

    ![alt text][image1] (image) I change the color space to H L S, it is much lighter and really easy to used

    ```
    cv2.cvtColor(img, cv2.COLOR_RGB2HLS)
    ```

- ### Resize

    ![alt text][image9]

    ```
    cv2.resize(img, (200, 125))
    ```
    I decide to resize because this will allow for faster computations as a smaller image is easier to work with.


![alt text][image3]
# U-NET model



```python
def unet(x, y, nbr_mask, size, padding):
    conv1 = conv2d(x, 3, 1, 32, padding, True)
    conv1 = conv2d(conv1, 3, 1, 32, padding, True)
    pool1 = max_pool(conv1, 2)

    conv2 = conv2d(pool1, 3, 1, 64, padding, True)
    conv2 = conv2d(conv2, 3, 1, 64, padding, True)
    pool2 = max_pool(conv2, 2)

    conv3 = conv2d(pool2, 3, 1, 128, padding, True)
    conv3 = conv2d(conv3, 3, 1, 128, padding, True)
    pool3 = max_pool(conv3, 2)

    conv4 = conv2d(pool3, 3, 1, 256, padding, True)
    conv5 = conv2d(conv4, 3, 1, 256, padding, True)

    deconv3 = conv2d_t(conv5, 3, 2, 256, padding, True)
    conv3 = crop(conv3, deconv3)
    concat1 = tf.concat((deconv3, conv3), axis=3)

    conv6 = conv2d(concat1, 3, 1, 128, padding, True)
    conv7 = conv2d(conv6, 3, 1, 128, padding, True)

    deconv2 = conv2d_t(conv7, 3, 2, 128, padding, True)
    conv2 = crop(conv2, deconv2)
    concat2 = tf.concat((deconv2, conv2), axis=3)

    conv8 = conv2d(concat2, 3, 1, 64, padding, True)
    conv9 = conv2d(conv8, 3, 1, 64, padding, True)

    deconv1 = conv2d_t(conv9, 3, 2, 64, padding, True)
    conv1 = crop(conv1, deconv1)
    concat3 = tf.concat((deconv1, conv1), axis=3)

    conv10 = conv2d(concat3, 3, 1, 32, padding, True)
    conv11 = conv2d(conv10, 3, 1, 32, padding, True)

    output = conv2d(conv11, 1, 1, nbr_mask, padding, False)
    output = tf.compat.v1.image.resize_images(output, (y.get_shape()[1], y.get_shape()[2]))
    mask = tf.nn.sigmoid(output, name='output')
    return output, mask
```
(image graph)

![alt text][image2]

(image loss)
![alt text][image3]


# Result

![alt text][image4]
![alt text][image4]
![alt text][image4]