import os
import sys
import numpy as np
from cv2 import cv2
import tensorflow as tf
sys.path.append('./../preprocess/')
import preprocess as pp

WIDTH = 200
HEIGHT = 125

#FOLDER = '../dataset/cat1/cropped_images/images/'
FOLDER = '../dataset/dataset_altran/DentalPanoramicXrays/Images/'

tab_color = [(255, 0, 0)]
tab_label = ['dents']
tab_value = [0.9]
tab_surface = [100000]

with tf.compat.v1.Session() as sess:
    saver = tf.compat.v1.train.import_meta_graph('./model/model.meta')
    saver.restore(sess, tf.train.latest_checkpoint('./model/'))
    graph = tf.compat.v1.get_default_graph()
    #all_names = [op.name for op in graph.get_operations()]
    Input = graph.get_tensor_by_name("input:0")
    Output = graph.get_tensor_by_name("output:0")
    folder = os.listdir(FOLDER)
    folder = sorted(folder)
    for pic in folder:
        img = cv2.imread(FOLDER+pic)
        image = pp.img_preprocess(img)
        multiply_y, multiply_x = int(img.shape[0]/HEIGHT), int(img.shape[1]/WIDTH)
        prediction = sess.run(Output, feed_dict={Input: [image]})
        for m in range(prediction[0].shape[-1]):
            mask = np.zeros(shape=(prediction[0].shape[0], prediction[0].shape[1]))
            mask[prediction[0][:, :, m] > tab_value[m]] = 1.
            mask = cv2.resize(mask, (multiply_x*WIDTH, multiply_y*HEIGHT))
            elements = cv2.findContours(mask.astype(np.uint8),
                                        cv2.RETR_EXTERNAL,
                                        cv2.CHAIN_APPROX_SIMPLE)[0]
            for e in elements:
                #print(cv2.contourArea(e))
                if cv2.contourArea(e) > tab_surface[m]:
                    x, y, w, h = cv2.boundingRect(e)
                    print("x : {}, y : {}, w : {}, h : {}".format(x, y, w, h))
                    cv2.putText(img,
                                tab_label[m],
                                (x, y-10),
                                cv2.FONT_HERSHEY_DUPLEX,
                                0.7,
                                tab_color[m],
                                2)
                    cv2.rectangle(img, (x, y), (x+w, y+h), tab_color[m], 5)
            #TODO polyfit for create exact location's teeth (create mask)
            #pts = np.round(np.asarray(prediction[0]))
            #cv2.fillPoly(img, [pts], (255, 0, 0)) #display mask check cv2.fillPoly()
        img = cv2.resize(img, (4*WIDTH, 4*HEIGHT))
        cv2.imshow(str(pic), img)
        mask = cv2.resize(mask, (4*WIDTH, 4*HEIGHT))
        cv2.imshow("mask", mask)
        #cv2.waitKey(0)

        key = cv2.waitKey()&0xFF
        if key == ord('q'):
            break
        if key == ord('n'):
            cv2.destroyAllWindows()
