#!/usr/bin python3
import sys
import ce_net as cenet
import u_net as unet
import tensorflow as tf
import tensorflow.compat.v1 as tf
from sklearn.model_selection import train_test_split
from termcolor import colored
sys.path.append('../preprocess/')
import preprocess as pp

tf.disable_v2_behavior()

EPOCHS = 5000
BATCH_SIZE = 100


X, Y, num_classes = pp.read_data()
print(X.shape)
print(Y.shape)

x = tf.keras.backend.placeholder(shape=(None, X.shape[1], X.shape[2], 3),
                                 dtype=tf.float32, name='input')
y = tf.keras.backend.placeholder(shape=(None, Y.shape[1], Y.shape[2], num_classes),
                                 dtype=tf.float32)
print(x.shape)
print(y.shape)


#model, mask = unet.unet(x, num_classes, (X.shape[1], Y.shape[2], 3), 'VALID')
model, mask = cenet.ce_net(x, num_classes, (X.shape[1], Y.shape[2], 3), 'VALID')
"""
with tf.Session() as sess:
    writer = tf.summary.FileWriter('logs', sess.graph)
    print(sess.run(model))
    writer.close()
"""

L_RATE = 0.001
cross_entropy = tf.nn.sigmoid_cross_entropy_with_logits(labels=y, logits = model)
#cross_entropy = tf.nn.softmax_cross_entropy_with_logits_v2(labels=y, logits=model)

loss_operation = tf.reduce_mean(cross_entropy)
optimizer = tf.compat.v1.train.AdamOptimizer(learning_rate=L_RATE)
training_operation = optimizer.minimize(loss_operation)
correct_prediction = tf.equal(tf.round(mask), y)
accuracy_operation = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

saver = tf.train.Saver()


X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=.2)


def evaluate(x_data, y_data):
    """
    param x_data : X_train or X_test
    param y_data : y_train or y_test

    return accuracy
    """
    num_examples = len(x_data)
    total_accuracy = 0
    sess = tf.get_default_session()
    for offset in range(0, num_examples, BATCH_SIZE):
        batch_x, batch_y = x_data[offset:offset+BATCH_SIZE], y_data[offset:offset+BATCH_SIZE]
        accuracy = sess.run(accuracy_operation, feed_dict={x: batch_x, y: batch_y})
        total_accuracy += (accuracy * len(batch_x))
        loss = sess.run(loss_operation, feed_dict={x: batch_x, y: batch_y})
        acc = total_accuracy / num_examples
    return acc


STOPPED_AT = 10
BEST_EPOCH = 0
BEST_VAL_ACC = 0
HISTORY = []
sess = tf.Session()

with sess as sess:
    sess.run(tf.global_variables_initializer())
    num_examples = len(X_train)
    print(colored("Training.......", "green", attrs=["bold", "blink"]))
    for i in range(EPOCHS):
        for offset in range(0, num_examples, BATCH_SIZE):
            end = offset + BATCH_SIZE
            batch_x, batch_y = X_train[offset:end], y_train[offset:end]
            sess.run(training_operation, feed_dict={x: batch_x, y: batch_y})

        validation_accuracy = evaluate(X_test, y_test)
        train_accuracy = evaluate(X_train, y_train)
        print("\nEPOCH {} ...".format(i+1))
        print("Training Accuracy = {}".format(train_accuracy))
        print("Validation Accuracy = {}".format(validation_accuracy))
        HISTORY.append([train_accuracy, validation_accuracy])
        if validation_accuracy > BEST_VAL_ACC:
            BEST_EPOCH, BEST_VAL_ACC = i, validation_accuracy
            #saver.save(sess, './model/model')
            print(colored("MODEL SAVE", "green"))
        else:
            if i - BEST_EPOCH == STOPPED_AT:
                print("best epochs {} with {}".format(BEST_EPOCH, BEST_VAL_ACC))
                print('Stopping after {} epochs without improvement'.format(STOPPED_AT))
                break
