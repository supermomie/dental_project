import tensorflow as tf
import tensor_functions as tfunc
import tensorflow.compat.v1 as tf

def unet(x, nbr_mask, size, padding):
    """
    param x        : the tensor input
    param nbr_mask : number of classes
    param size     : picture's size
    param paddinf  : padding selection

    return output and mask
    """
    conv1 = tfunc.conv2d(x, 3, 1, 32, padding, True)
    conv1 = tfunc.conv2d(conv1, 3, 1, 32, padding, True)
    pool1 = tfunc.max_pool(conv1, 2)

    conv2 = tfunc.conv2d(pool1, 3, 1, 64, padding, True)
    conv2 = tfunc.conv2d(conv2, 3, 1, 64, padding, True)
    pool2 = tfunc.max_pool(conv2, 2)

    conv3 = tfunc.conv2d(pool2, 3, 1, 128, padding, True)
    conv3 = tfunc.conv2d(conv3, 3, 1, 128, padding, True)
    pool3 = tfunc.max_pool(conv3, 2)

    conv4 = tfunc.conv2d(pool3, 3, 1, 256, padding, True)
    conv5 = tfunc.conv2d(conv4, 3, 1, 256, padding, True)

    deconv3 = tfunc.conv2d_t(conv5, 3, 2, 256, padding, True)
    conv3 = tfunc.crop(conv3, deconv3)
    concat1 = tf.concat((deconv3, conv3), axis=3)

    conv6 = tfunc.conv2d(concat1, 3, 1, 128, padding, True)
    conv7 = tfunc.conv2d(conv6, 3, 1, 128, padding, True)

    deconv2 = tfunc.conv2d_t(conv7, 3, 2, 128, padding, True)
    conv2 = tfunc.crop(conv2, deconv2)
    concat2 = tf.concat((deconv2, conv2), axis=3)

    conv8 = tfunc.conv2d(concat2, 3, 1, 64, padding, True)
    conv9 = tfunc.conv2d(conv8, 3, 1, 64, padding, True)

    deconv1 = tfunc.conv2d_t(conv9, 3, 2, 64, padding, True)
    conv1 = tfunc.crop(conv1, deconv1)
    concat3 = tf.concat((deconv1, conv1), axis=3)

    conv10 = tfunc.conv2d(concat3, 3, 1, 32, padding, True)
    conv11 = tfunc.conv2d(conv10, 3, 1, 32, padding, True)

    output = tfunc.conv2d(conv11, 1, 1, nbr_mask, padding, False)
    output = tf.compat.v1.image.resize_images(output, (x.get_shape()[1], x.get_shape()[2]))
    mask = tf.nn.sigmoid(output, name='output')
    return output, mask


