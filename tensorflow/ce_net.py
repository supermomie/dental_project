import tensorflow as tf
import tensor_functions as tfunc
import tensorflow.compat.v1 as tf

def ce_net(x, nbr_mask, size, padding):
    """
    param x        : the tensor input
    param nbr_mask : number of classes
    param size     : picture's size
    param padding  : padding selection
    """
    scale_img_2 = tf.keras.layers.AveragePooling2D(pool_size=(2, 2))(x)
    scale_img_3 = tf.keras.layers.AveragePooling2D(pool_size=(2, 2))(scale_img_2)
    scale_img_4 = tf.keras.layers.AveragePooling2D(pool_size=(2, 2))(scale_img_3)

    conv1 = tfunc.conv2d(x, 3, 1, 32, "SAME", True)
    conv1 = tfunc.conv2d(conv1, 3, 1, 32, "SAME", True)
    pool1 = tfunc.max_pool2d(conv1, 2)

    input2 = tfunc.conv2d(scale_img_2, 3, 1, 64, "SAME", True)
    input2 = tf.concat((input2, pool1), axis=3)
    conv2 = tfunc.conv2d(input2, 3, 1, 64, "SAME", True)
    conv2 = tfunc.conv2d(conv2, 3, 1, 64, "SAME", True)
    pool2 = tfunc.max_pool2d(conv2, 2)


    input3 = tfunc.conv2d(scale_img_3, 3, 1, 128, "SAME", True)
    input3 = tf.concat((input3, pool2), axis=3)
    conv3 = tfunc.conv2d(input3, 3, 1, 128, "SAME", True)
    conv3 = tfunc.conv2d(conv3, 3, 1, 128, "SAME", True)
    pool3 = tfunc.max_pool2d(conv3, 2)

    input4 = tfunc.conv2d(scale_img_4, 3, 1, 256, "SAME", True)
    input4 = tf.concat((input4, pool3), axis=3)
    conv4 = tfunc.conv2d(input4, 3, 1, 256, "SAME", True)
    conv4 = tfunc.conv2d(conv4, 3, 1, 256, "SAME", True)
    pool4 = tfunc.max_pool2d(conv4, 2)

    conv5 = tfunc.conv2d(pool4, 3, 1, 512, "SAME", True)
    conv5 = tfunc.conv2d(conv5, 3, 1, 512, "SAME", True)

    deconv4 = tfunc.conv2d_t(conv5, 2, 2, 256, "SAME", True)
    conv4 = tfunc.crop(conv4, deconv4)
    up6 = tf.concat((deconv4, conv4), axis=3)

    conv6 = tfunc.conv2d(up6, 3, 1, 256, "SAME", True)
    conv6 = tfunc.conv2d(conv6, 3, 1, 256, "SAME", True)

    deconv3 = tfunc.conv2d_t(conv6, 2, 2, 256, "SAME", True)
    conv3 = tfunc.crop(conv3, deconv3)
    up7 = tf.concat((deconv3, conv3), axis=3)

    conv7 = tfunc.conv2d(up7, 3, 1, 128, "SAME", True)
    conv7 = tfunc.conv2d(conv7, 3, 1, 128, "SAME", True)

    deconv2 = tfunc.conv2d_t(conv7, 2, 2, 64, "SAME", True)
    conv2 = tfunc.crop(conv2, deconv2)
    up8 = tf.concat((deconv2, conv2), axis=3)

    conv8 = tfunc.conv2d(up8, 3, 1, 64, "SAME", True)
    conv8 = tfunc.conv2d(conv8, 3, 1, 64, "SAME", True)

    deconv1 = tfunc.conv2d_t(conv8, 2, 2, 32, "SAME", True)
    conv1 = tfunc.crop(conv1, deconv1)
    up9 = tf.concat((deconv1, conv1), axis=3)

    conv9 = tfunc.conv2d(up9, 3, 1, 32, "SAME", True)
    conv9 = tfunc.conv2d(conv9, 3, 1, 32, "SAME", True)

    side6 = tf.keras.layers.UpSampling2D(size=(8, 8))(conv6)
    side7 = tf.keras.layers.UpSampling2D(size=(4, 4))(conv7)
    side8 = tf.keras.layers.UpSampling2D(size=(2, 2))(conv8)

    out1 = tfunc.conv2d(side6, 8, 1, 1, "VALID", True)
    out1 = tf.nn.sigmoid(out1, name='output1')
    out2 = tfunc.conv2d(side7, 4, 1, 1, "VALID", True)
    out2 = tf.nn.sigmoid(out2, name='output2')
    out3 = tfunc.conv2d(side8, 2, 1, 1, "VALID", True)
    out3 = tf.nn.sigmoid(out3, name='output3')
    out4 = tfunc.conv2d(conv9, 1, 1, 1, "VALID", True)
    out4 = tf.nn.sigmoid(out4, name='output4')
    #output = tf.keras.layers.Average()([out1, out2, out3, out4])
    output = tfunc.conv2d(conv9, 1, 1, nbr_mask, padding, False)
    output = tf.compat.v1.image.resize_images(output, (x.get_shape()[1], x.get_shape()[2]))
    mask = tf.nn.sigmoid(output, name='output')
    return output, mask
