import tensorflow as tf



def dropout(conv, rate):
    """
    return dropout
    """
    drop = tf.nn.dropout(conv, rate, noise_shape=None, seed=None, name=None)
    return drop



def crop(tensor1, tensor2):
    """
    return tensor cropped
    """
    offsets = (0,
               (int(tensor1.get_shape()[1]) - int(tensor2.get_shape()[1]))//2,
               (int(tensor1.get_shape()[2]) - int(tensor2.get_shape()[2]))//2,
               0)
    size = (-1, int(tensor2.get_shape()[1]), int(tensor2.get_shape()[2]), -1)
    return tf.slice(tensor1, offsets, size)



def max_pool(conv, pool_size):
    """
    return the pool
    """
    pool = tf.nn.max_pool(conv,
                          ksize=[1, pool_size, pool_size, 1],
                          strides=[1, pool_size, pool_size, 1],
                          padding='SAME')
    return pool



def max_pool2d(conv, pool_size):
    """
    return the pool2d
    """
    pool2d = tf.nn.max_pool2d(conv,
                              ksize=[1, pool_size, pool_size, 1],
                              strides=[1, pool_size, pool_size, 1],
                              padding='VALID')
    return pool2d



def conv2d(convul, kernel, stride, out, padding, norm):
    """
    return conv
    """
    shape = (kernel, kernel, convul.get_shape()[-1], out)
    print("shape :", shape)
    conv_w = tf.Variable(tf.random.truncated_normal(shape=shape, dtype=tf.float32))
    conv_b = tf.Variable(tf.zeros(out))
    conv = tf.nn.conv2d(convul, conv_w, strides=[1, stride, stride, 1], padding=padding) + conv_b
    conv = tf.nn.bias_add(conv, conv_b)
    if norm is True:
        conv = tf.compat.v1.layers.batch_normalization(conv, training=norm) 
        # FIXME: change for native tf v2
        conv = tf.nn.relu(conv)
    return conv


def conv2d_t(convul, kernel, stride, out, padding, norm):
    """
    return deconvolution
    """
    shape = (kernel, kernel, out, convul.get_shape()[-1])
    print("shape_T :", shape)
    conv_w = tf.Variable(tf.random.truncated_normal(shape=shape, dtype=tf.float32))
    conv_b = tf.Variable(tf.zeros(out))
    if padding == 'VALID':
        out_h = (convul.get_shape()[1]-1)*stride+kernel
        out_w = (convul.get_shape()[2]-1)*stride+kernel
    elif padding == 'SAME':
        out_h = (convul.get_shape()[1]-1)*stride+1
        out_w = (convul.get_shape()[2]-1)*stride+1
    else:
        print("err padding param name")
        exit()
    b_size = tf.shape(convul)[0]
    conv_t = tf.nn.conv2d_transpose(convul,
                                    conv_w,
                                    output_shape=[b_size, out_h, out_w, out],
                                    strides=[1, stride, stride, 1],
                                    padding=padding)
    conv_t = tf.nn.bias_add(conv_t, conv_b)
    if norm is True:
        conv_t = tf.compat.v1.layers.batch_normalization(conv_t, training=norm) #TODO change for native tf v2
        conv_t = tf.nn.relu(conv_t)
    return conv_t
