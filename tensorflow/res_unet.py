import tensorflow as tf
import tensor_functions as tfunc
import tensorflow.compat.v1 as tf

def res_unet(x, nbr_mask, size, padding):
    """
    param x        : the tensor input
    param nbr_mask : number of classes
    param size     : picture's size
    param paddinf  : padding selection

    return output and mask
    """
    #FIXME to ajust because it seems to doesn't take rectangle picture
    # first encoder
    conv1 = tfunc.conv2d(x, 3, 1, 32, padding, True)
    conv1 = tfunc.conv2d(conv1, 3, 1, 32, padding, True)
    pool1 = tfunc.max_pool(conv1, 2)

    conv2 = tfunc.conv2d(pool1, 3, 1, 64, padding, True)
    conv2 = tfunc.conv2d(conv2, 3, 1, 64, padding, True)
    pool2 = tfunc.max_pool(conv2, 2)

    conv3 = tfunc.conv2d(pool2, 3, 1, 128, padding, True)
    conv3 = tfunc.conv2d(conv3, 3, 1, 128, padding, True)
    pool3 = tfunc.max_pool(conv3, 2)

    conv4 = tfunc.conv2d(pool3, 3, 1, 256, padding, True)
    conv4 = tfunc.conv2d(conv4, 3, 1, 256, padding, True)
    pool4 = tfunc.max_pool2d(conv4, 2)

    conv5 = tfunc.conv2d(pool4, 3, 1, 512, padding, True)
    conv5 = tfunc.conv2d(conv5, 3, 1, 512, padding, True)
    conv5 = tfunc.dropout(conv5, 0.5)

    # second encoder
    conv1_t = tfunc.conv2d(x, 3, 1, 32, padding, True)
    conv1_t = tfunc.conv2d(conv1_t, 3, 1, 32, padding, True)
    pool1_t = tfunc.max_pool(conv1_t, 2)

    conv2_t = tfunc.conv2d(pool1_t, 3, 1, 64, padding, True)
    conv2_t = tfunc.conv2d(conv2_t, 3, 1, 64, padding, True)
    pool2_t = tfunc.max_pool(conv2_t, 2)

    conv3_t = tfunc.conv2d(pool2_t, 3, 1, 128, padding, True)
    conv3_t = tfunc.conv2d(conv3_t, 3, 1, 128, padding, True)
    pool3_t = tfunc.max_pool(conv3_t, 2)

    conv4_t = tfunc.conv2d(pool3_t, 3, 1, 256, padding, True)
    conv4_t = tfunc.conv2d(conv4_t, 3, 1, 256, padding, True)
    pool4_t = tfunc.max_pool2d(conv4_t, 2)

    conv5_t = tfunc.conv2d(pool4_t, 3, 1, 512, padding, True)
    conv5_t = tfunc.conv2d(conv5_t, 3, 1, 512, padding, True)
    conv5_t = tfunc.dropout(conv5_t, 0.5)

    concat1 = tf.concat((conv5, conv5_t), axis=3)


    conv1_m = tfunc.conv2d(concat1, 3, 1, 256, padding, True)
    conv1_m = tf.keras.layers.UpSampling2D(size=(2, 2))(conv1_m)
    concat2 = tf.keras.layers.concatenate([conv4, conv4_t, conv1_m], axis=3)
    conv1_m = tfunc.conv2d(concat2, 3, 1, 256, padding, True)
    conv1_m = tfunc.conv2d(conv1_m, 3, 1, 256, padding, True)

    conv2_m = tfunc.conv2d(conv1_m, 3, 1, 128, padding, True)
    conv2_m = tf.keras.layers.UpSampling2D(size=(2, 2))(conv2_m)
    concat3 = tf.keras.layers.concatenate([conv3, conv3_t, conv2_m], axis=3)
    conv2_m = tfunc.conv2d(concat3, 3, 1, 128, padding, True)
    conv2_m = tfunc.conv2d(conv2_m, 3, 1, 128, padding, True)

    conv3_m = tfunc.conv2d(conv2_m, 3, 1, 64, padding, True)
    conv3_m = tf.keras.layers.UpSampling2D(size=(2, 2))(conv3_m)
    concat4 = tf.keras.layers.concatenate([conv2, conv2_t, conv3_m], axis=3)
    conv3_m = tfunc.conv2d(concat4, 3, 1, 64, padding, True)
    conv3_m = tfunc.conv2d(conv3_m, 3, 1, 64, padding, True)

    conv4_m = tfunc.conv2d(conv3_m, 3, 1, 32, padding, True)
    conv4_m = tf.keras.layers.UpSampling2D(size=(2, 2))(conv4_m)
    concat5 = tf.keras.layers.concatenate([conv1, conv1_t, conv4_m], axis=3)
    conv4_m = tfunc.conv2d(concat5, 3, 1, 32, padding, True)
    conv4_m = tfunc.conv2d(conv4_m, 3, 1, 32, padding, True)

    conv5_m = tfunc.conv2d(conv4_m, 3, 1, 6, padding, True)

    output = tfunc.conv2d(conv5_m, 1, 1, nbr_mask, padding, False)
    output = tf.compat.v1.image.resize_images(output, (x.get_shape()[1], x.get_shape()[2]))
    mask = tf.nn.sigmoid(output, name='output')
    return output, mask
